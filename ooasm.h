#ifndef OOASM_LIBRARY_H
#define OOASM_LIBRARY_H

#include "ooasm_implementation.h"

inline std::unique_ptr<Num> num(wordT value) {
    return std::make_unique<Num>(value);
}

inline std::unique_ptr<Lea> lea(idT id) {
    return std::make_unique<Lea>(id);
}

inline std::unique_ptr<Mem> mem(std::unique_ptr<rValue> address) {
    return std::make_unique<Mem>(std::move(address));
}

inline std::shared_ptr<Data> data(idT id, std::unique_ptr<Num> value) {
    return std::make_shared<Data>(id, value->value);
}

inline std::shared_ptr<Mov> mov(std::unique_ptr<lValue> dst, std::unique_ptr<rValue> src) {
    return std::make_shared<Mov>(std::move(dst), std::move(src));
}

inline std::shared_ptr<AddInstruction> add(std::unique_ptr<lValue> arg1, 
                                    std::unique_ptr<rValue> arg2) {
    return std::make_shared<AddInstruction>(std::move(arg1), std::move(arg2));
}

inline std::shared_ptr<SubtractInstruction> sub(std::unique_ptr<lValue> arg1, 
                                         std::unique_ptr<rValue> arg2) {
    return std::make_shared<SubtractInstruction>(std::move(arg1), std::move(arg2));
}

inline std::shared_ptr<IncrementInstruction> inc(std::unique_ptr<lValue> arg1) {
    return std::make_shared<IncrementInstruction>(std::move(arg1));
}

inline std::shared_ptr<DecrementInstruction> dec(std::unique_ptr<lValue> arg1) {
    return std::make_shared<DecrementInstruction>(std::move(arg1));
}

inline std::shared_ptr<ConditionalAssignment> one(std::unique_ptr<lValue> arg) {
    return std::make_shared<ConditionalAssignment>(std::move(arg));
}

inline std::shared_ptr<ZeroConditionalAssignment> onez(std::unique_ptr<lValue> arg) {
    return std::make_shared<ZeroConditionalAssignment>(std::move(arg));
}

inline std::shared_ptr<SignConditionalAssignment> ones(std::unique_ptr<lValue> arg) {
    return std::make_shared<SignConditionalAssignment>(std::move(arg));
}

class program {
public:
    program(std::initializer_list<std::shared_ptr<Instruction>> instructions)
            : instructions(instructions) {}

    void declare_variables(ComputerState &cs) const {
        for (auto const &ins : instructions)
            ins->declare(cs);
    }

    void execute(ComputerState &cs) const {
        for (auto const &ins : instructions)
            ins->execute(cs);
    }

private:
    std::vector<std::shared_ptr<Instruction>> instructions;
};

#endif //OOASM_LIBRARY_H
