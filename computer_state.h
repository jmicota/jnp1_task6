#ifndef OOASM_COMPUTER_STATE_H
#define OOASM_COMPUTER_STATE_H

#include <vector>
#include <cstring>
#include <cstdint>
#include <stdexcept>

using wordT = int64_t;
using addressT = uint64_t;
using idT = const char*;

class Memory {
public:
    explicit Memory(std::vector<wordT> &memory) : memory(memory) {}

    [[nodiscard]] wordT get_value_at(const addressT address) const {
        if (address >= memory.size())
            throw std::out_of_range("index out of range");

        return memory[address];
    }

    void set_value(const addressT address, const wordT value) {
        if (address >= memory.size())
            throw std::out_of_range("index out of range");

        memory[address] = value;
    }

    wordT get_variable_address(const idT id) const {
        for (addressT i = 0; i < variables.size(); i++) {
            if (strcmp(variables[i], id) == 0) {
                return i;
            }
        }
        throw std::invalid_argument("no variable with this id has been declared");
    }

    void declare_variable(const idT id, const wordT value) {
        if (variables.size() == memory.size())
            throw std::bad_alloc();
            
        memory[variables.size()] = value;
        variables.push_back(id);
    }

private:
    std::vector<wordT> &memory;
    std::vector<idT> variables;
};

class Processor {
public:
    explicit Processor() : SF(false), ZF(false) {}

    void set_flags(const wordT value) {
        ZF = (value == 0);
        SF = (value < 0);
    }

    [[nodiscard]] bool zero_flag() const { return ZF; }
    [[nodiscard]] bool sign_flag() const { return SF; }

private:
    bool SF;
    bool ZF;
};

class ComputerState {
public:
    explicit ComputerState(std::vector<wordT> &memory_arg)
                           : memory(std::make_unique<Memory>(memory_arg)), 
                             processor(std::make_unique<Processor>()) {}

    [[nodiscard]] wordT get_value_at(const addressT address) const {
        return memory->get_value_at(address);
    }

    void set_value(const addressT address, const wordT value) {
        return memory->set_value(address, value);
    }

    wordT get_variable_address(const idT id) const {
        return memory->get_variable_address(id);
    }

    void declare_variable(const idT id, const wordT value) {
        return memory->declare_variable(id, value);
    }

    void set_flags(const wordT value) {
        processor->set_flags(value);
    }

    [[nodiscard]] bool zero_flag() const { return processor->zero_flag(); }
    [[nodiscard]] bool sign_flag() const { return processor->sign_flag(); }

private:
    std::unique_ptr<Memory> memory{};
    std::unique_ptr<Processor> processor{};
};

#endif //OOASM_COMPUTER_STATE_H
