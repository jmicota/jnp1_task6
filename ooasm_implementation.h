#ifndef OOASM_IMPL_LIBRARY_H
#define OOASM_IMPL_LIBRARY_H

#include <memory>
#include <iostream>
#include "computer_state.h"

class rValue {
public:
    virtual ~rValue() = default;

    virtual wordT get_rvalue(ComputerState &cs) const = 0;
};

class lValue {
public:
    virtual ~lValue() = default;

    virtual addressT get_lvalue(ComputerState &cs) const = 0;
};

class Id {
public:
    explicit Id(idT id_arg) {
        if (id_arg == nullptr)
            throw std::invalid_argument("null id");

        int len = 0;
        while (id_arg[len] != '\0') {
            if (len > 9)
                throw std::invalid_argument("id too long");
            ++len;
        }
        if (len == 0)
            throw std::invalid_argument("id must contain at least one character");

        id = id_arg;
    }

    [[nodiscard]] idT get_id() const { return id; }

private:
    idT id;
};

class Num : public rValue {
public:
    explicit Num(wordT value): value(value) {}

    wordT get_rvalue(ComputerState &) const override { return value; }

    wordT value;
};

class Lea : public rValue {
public:
    explicit Lea(idT id_arg) : id(std::make_unique<Id>(id_arg)) {}

    wordT get_rvalue(ComputerState &cs) const override {
        return cs.get_variable_address(id->get_id());
    }

private:
    std::unique_ptr<Id> id;
};

class Mem : public lValue, public rValue {
public:
    explicit Mem(std::unique_ptr<rValue> address) : address(std::move(address)) {}

    addressT get_lvalue(ComputerState &cs) const override {
        return address->get_rvalue(cs);
    }

    wordT get_rvalue(ComputerState &cs) const override {
        return cs.get_value_at(get_lvalue(cs));
    }

private:
    std::unique_ptr<rValue> address;
};

class Instruction {
public:
    virtual ~Instruction() = default;

    virtual void declare(ComputerState &) const {}
    virtual void execute(ComputerState &cs) const = 0;
};

class Mov : public Instruction {
public:
    Mov(std::unique_ptr<lValue> dst, std::unique_ptr<rValue> src) : dst(std::move(dst)),
                                                                    src(std::move(src)) {}

    void execute(ComputerState &cs) const override {
        cs.set_value(dst->get_lvalue(cs), src->get_rvalue(cs));
    }

private:
    std::unique_ptr<lValue> dst;
    std::unique_ptr<rValue> src;
};

class Data : public Instruction {
public:
    Data(idT id_arg, wordT value) : id(std::make_unique<Id>(id_arg)), value(value) {}

    void declare(ComputerState &cs) const override {
        cs.declare_variable(id->get_id(), value);
    }

    void execute(ComputerState &) const override {}
private:
    std::unique_ptr<Id> id;
    wordT value;
};

class ArithmeticInstruction : public Instruction {
public:
    ArithmeticInstruction(std::unique_ptr<lValue> arg1, std::unique_ptr<rValue> arg2)
            : arg1(std::move(arg1)), arg2(std::move(arg2)) {}

protected:
    std::unique_ptr<lValue> arg1;
    std::unique_ptr<rValue> arg2;
};

class AddInstruction : public ArithmeticInstruction {
public:
    AddInstruction(std::unique_ptr<lValue> arg1, std::unique_ptr<rValue> arg2)
            : ArithmeticInstruction(std::move(arg1), std::move(arg2)) {}

    void execute(ComputerState &cs) const override {
        addressT address = arg1->get_lvalue(cs);
        wordT result = cs.get_value_at(address) + arg2->get_rvalue(cs);

        cs.set_flags(result);
        cs.set_value(address, result);
    }

};

class IncrementInstruction : public AddInstruction {
public:
    explicit IncrementInstruction(std::unique_ptr<lValue> arg1)
            : AddInstruction(std::move(arg1), std::make_unique<Num>(1)) {}
};

class SubtractInstruction : public ArithmeticInstruction {
public:
    SubtractInstruction(std::unique_ptr<lValue> arg1, std::unique_ptr<rValue> arg2)
            : ArithmeticInstruction(std::move(arg1), std::move(arg2)) {}

    void execute(ComputerState &cs) const override {
        addressT address = arg1->get_lvalue(cs);
        wordT result = cs.get_value_at(address) - arg2->get_rvalue(cs);

        cs.set_flags(result);
        cs.set_value(address, result);
    }
};

class DecrementInstruction: public SubtractInstruction {
public:
    explicit DecrementInstruction(std::unique_ptr<lValue> arg1)
            : SubtractInstruction(std::move(arg1), std::make_unique<Num>(1)) {}
};

class ConditionalAssignment : public Instruction {
public:
    explicit ConditionalAssignment(std::unique_ptr<lValue> arg): arg(std::move(arg)) {}

    void execute(ComputerState &cs) const override {
        if (checkCondition(cs)) {
            addressT address = arg->get_lvalue(cs);
            cs.set_value(address, 1);
        }
    }

    [[nodiscard]] virtual bool checkCondition(ComputerState &) const { return true; };

private:
    std::unique_ptr<lValue> arg;
};

class ZeroConditionalAssignment : public ConditionalAssignment {
public:
    explicit ZeroConditionalAssignment(std::unique_ptr<lValue> arg)
            : ConditionalAssignment(std::move(arg)) {}

    [[nodiscard]] bool checkCondition(ComputerState &cs) const override {
        return cs.zero_flag();
    }
};

class SignConditionalAssignment : public ConditionalAssignment {
public:
    explicit SignConditionalAssignment(std::unique_ptr<lValue> arg)
            : ConditionalAssignment(std::move(arg)) {}

    [[nodiscard]] bool checkCondition(ComputerState &cs) const override {
        return cs.sign_flag();
    }
};

#endif //OOASM_IMPL_LIBRARY_H