#ifndef OOASM_COMPUTER_H
#define OOASM_COMPUTER_H

#include <ostream>
#include "ooasm.h"

class Computer {
public:
    explicit Computer(addressT memory_size): memory_size(memory_size) {}

    void boot(program &program) {
        memory = std::vector<wordT>(memory_size, 0);
        ComputerState cs = ComputerState(memory);
        program.declare_variables(cs);
        program.execute(cs);
    }

    void memory_dump (std::ostream &stream) const {
        for (wordT word: memory)
            stream << word << " ";
    }

private:
    addressT memory_size;
    std::vector<wordT> memory;
};

#endif //OOASM_COMPUTER_H
